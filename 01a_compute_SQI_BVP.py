#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#%%
import pyphysio as ph
import os
import numpy as np
import scipy.stats as sps
from config import DATASET_FOLDER

#%%
def compute_kSQI(x):
    return(sps.kurtosis(x))

def compute_sSQI(x):
    return(sps.skew(x))
    
def compute_pSQI(x):
    p_1_2 = ph.PowerInBand(method='welch', freq_min=1, freq_max=2.25)(x)
    p_0_8 = ph.PowerInBand(method='welch', freq_min=0, freq_max=8)(x)
    return(p_1_2/p_0_8)

def compute_energy(x):
    return(np.sqrt(np.nanmean(np.power(np.diff(x), 2))))
    
def compute_sqis(signal, win_len=5, win_step=5):
    sqilist = [compute_kSQI, compute_sSQI, compute_pSQI, compute_energy]
    
    signal = ph.Normalize()(signal)
    fsamp = signal.get_sampling_freq()
    idx_len = win_len * fsamp
    idx_step = win_step * fsamp

    windows = np.arange(0, len(signal) - idx_len + 1, idx_step)

    sqis = []
    for i in range(1, len(windows) + 1):
        start = windows[i - 1]
        portion_curr = signal.segment_idx(start, start + idx_len)
        sqis.append( [sqi(portion_curr) for sqi in sqilist])
    
    sqis = np.array(sqis)
    return(sqis)

#%%
subjects = os.listdir(os.path.join(DATASET_FOLDER, 'signals'))
    
bp_filter = ph.IIRFilter(fp = [0.5, 50], fs = [0.05, 55], ftype='ellip')

#%%
PORTION = 'baseline'
OUTDIR_FC = os.path.join(DATASET_FOLDER, 'SQI', 'BVP', PORTION, 'FC')
OUTDIR_E4 = os.path.join(DATASET_FOLDER, 'SQI', 'BVP', PORTION, 'E4')
os.makedirs(OUTDIR_FC, exist_ok=True)
os.makedirs(OUTDIR_E4, exist_ok=True)

#%%
for SUB in subjects:
    #%
    DATAFILE_FC = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'bvp_FC.pkl')
    DATAFILE_E4 = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'bvp_E4.pkl')
    
    bvp_FC = ph.from_pickle(DATAFILE_FC)
    bvp_E4 = ph.from_pickle(DATAFILE_E4)
    
    # preprocessing
    bvp_FC = bp_filter(bvp_FC.resample(128))
    bvp_E4 = bp_filter(bvp_E4.resample(128))
        
    #
    sqi_fc = compute_sqis(bvp_FC)
    sqi_e4 = compute_sqis(bvp_E4)
    
    np.savetxt(os.path.join(OUTDIR_FC, f'{SUB}.txt'), sqi_fc)
    np.savetxt(os.path.join(OUTDIR_E4, f'{SUB}.txt'), sqi_e4)

#%%