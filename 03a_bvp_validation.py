# Compare ibi reference with estimates
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os 
import pyphysio as ph

from config import DATASET_FOLDER

#%%
def compare_ibi(ibi_1, ibi_2, B):

    t_ibi_1 = ibi_1.get_times()
    t_ibi_2 = ibi_2.get_times()
    
    # count TP, FN
    TPs_paired = []
    FNs = []
    selected = []
    for id_true, curr_t_ibi_1 in enumerate(t_ibi_1):
        
        id_candidate = np.argmin(abs(t_ibi_2 - curr_t_ibi_1))
        cand_t_ibi_2 = t_ibi_2[id_candidate]
        
        if id_candidate not in selected and abs(cand_t_ibi_2 - curr_t_ibi_1)<=B:
            TPs_paired.append([id_true, id_candidate])
            selected.append(id_candidate)
            #update times
            offset = t_ibi_1[id_true] - t_ibi_2[id_candidate]
            t_ibi_2 = t_ibi_2  + offset
        else:
            FNs.append(id_true)
    
    TPs_paired = np.array(TPs_paired)
    FNs = np.array(FNs)
    
    if np.shape(TPs_paired)[0] == len(t_ibi_1):
        FPs = []
    else:
        # count FP
        FPs = []
        id_est_true = TPs_paired[:,1]
        for id_est in range(len(t_ibi_2)):
            if id_est not in id_est_true:
                FPs.append(id_est)
        
    FPs = np.array(FPs)
    error = ibi_2.get_values()[TPs_paired[:,1]] - ibi_1.get_values()[TPs_paired[:,0]]
    
    rmse = np.sqrt( np.mean(error**2) )
    
    #compute metrics
    TP = np.shape(TPs_paired)[0]
    FN = len(FNs)
    FP = len(FPs)
    
    precision = TP / (TP+FP)
    recall = TP / (TP+FN)

    return(TPs_paired, FNs, FPs, rmse, precision, recall)

#%%
OUTDIR = os.path.join(DATASET_FOLDER, 'results')

subjects = os.listdir(os.path.join(DATASET_FOLDER, 'signals'))

#%%
metrics_all_b = []
PORTION = 'baseline'

for SUB in subjects:
    
    REF_IBI = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'ibi_FC.pkl')
    COMP_BVP = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'bvp_E4.pkl')
    
    ibi_ref = ph.from_pickle(REF_IBI)
    
    ecg_comp = ph.from_pickle(COMP_BVP)
    ibi_comp = ph.BeatFromBP()(ecg_comp)
    id_bad_ibi_comp = ph.BeatOutliers()(ibi_comp)
    if len(id_bad_ibi_comp)>0:
        ibi_comp = ph.FixIBI(id_bad_ibi_comp)(ibi_comp)

    TPs_paired, FNs, FPs, TPrmse, precision, recall = compare_ibi(ibi_ref, ibi_comp, 0.5)
    
    metrics_all_b.append(np.array([precision, recall, TPrmse]))

metrics_all_b = np.array(metrics_all_b)

#%%
metrics_all_m = []
PORTION = 'movement'

for SUB in subjects:
    
    REF_IBI = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'ibi_FC.pkl')
    COMP_BVP = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'bvp_E4.pkl')
    
    ibi_ref = ph.from_pickle(REF_IBI)
    
    ecg_comp = ph.from_pickle(COMP_BVP)
    ibi_comp = ph.BeatFromBP()(ecg_comp)
    id_bad_ibi_comp = ph.BeatOutliers()(ibi_comp)
    if len(id_bad_ibi_comp)>0:
        ibi_comp = ph.FixIBI(id_bad_ibi_comp)(ibi_comp)

    TPs_paired, FNs, FPs, TPrmse, precision, recall = compare_ibi(ibi_ref, ibi_comp, 0.5)
    
    metrics_all_m.append(np.array([precision, recall, TPrmse]))

metrics_all_m = np.array(metrics_all_m)

#%%
plt.figure()
plt.title('Precision')
plt.boxplot([metrics_all_b[:,0], metrics_all_m[:,0]], labels=['baseline', 'movement'])
plt.ylim((0, 1.2))
plt.grid()

plt.figure()
plt.title('Recall')
plt.boxplot([metrics_all_b[:,1], metrics_all_m[:,1]], labels=['baseline', 'movement'])
plt.ylim((0, 1.2))
plt.grid()

plt.figure()
plt.title('RMSE')
plt.boxplot([metrics_all_b[:,2], metrics_all_m[:,2]], labels=['baseline', 'movement'])
plt.ylabel('Seconds')
plt.grid()

#%%
np.savetxt(os.path.join(OUTDIR, 'BVP_ibi_baseline.txt'), metrics_all_b)
np.savetxt(os.path.join(OUTDIR, 'BVP_ibi_movement.txt'), metrics_all_m)