#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#%%
import pyphysio as ph
import os
import numpy as np
import scipy.stats as sps
from config import DATASET_FOLDER

#%%
def compute_orSQI(x, th_l= 0.05, th_h= 60):
    idx_or = np.where((x<th_l) | (x>th_h))[0]
    return(len(idx_or)/len(x))
    
def compute_jSQI(x, max_dx= 10):
    fsamp = x.get_sampling_freq()
    dt = 1/fsamp
    
    x = ph.IIRFilter(fp=0.05, fs=0.01)(x)
    dx = abs(np.diff(x)/dt)/np.std(x)
    
    idx_jumps = np.where(dx>max_dx)[0]
    
    return(len(idx_jumps))

def compute_energy(x, k_dx= 5):
    x = ph.IIRFilter(fp=0.05, fs=0.01)(x)
    return(np.sqrt(np.mean(sum(x**2))))
    
def compute_rSQI(x):
    x = ph.IIRFilter(fp=0.05, fs=0.01)(x)
    return(np.max(x) - np.min(x))

def compute_sqis(signal, win_len=20, win_step=20):
    
    t_st = signal.get_start_time()
    t_sp = t_st + win_len

    sqis = []
    while t_sp < signal.get_end_time():
        portion_curr = signal.segment_time(t_st, t_sp)
        or_curr = compute_orSQI(portion_curr)
        j_curr = compute_jSQI(portion_curr)
        e_curr = compute_energy(portion_curr)
        r_curr = compute_rSQI(portion_curr)
        
        sqis.append(np.array([or_curr, j_curr, e_curr, r_curr]))
        t_st +=win_step
        t_sp +=win_step
    return(np.array(sqis))

#%%
subjects = os.listdir(os.path.join(DATASET_FOLDER, 'signals'))

lp_filter = ph.IIRFilter(fp=1.5, fs=1.8)

#%%
PORTION = 'baseline'

OUTDIR_FC = os.path.join(DATASET_FOLDER, 'SQI', 'EDA', PORTION, 'FC')
OUTDIR_E4 = os.path.join(DATASET_FOLDER, 'SQI', 'EDA', PORTION, 'E4')
os.makedirs(OUTDIR_FC, exist_ok=True)
os.makedirs(OUTDIR_E4, exist_ok=True)

#%%
for SUB in subjects:
    #%
    DATAFILE_FC = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'eda_FC.pkl')
    DATAFILE_E4 = os.path.join(DATASET_FOLDER, 'signals', SUB, PORTION, 'eda_E4.pkl')

    #
    eda_FC = ph.from_pickle(DATAFILE_FC).resample(4)
    eda_E4 = ph.from_pickle(DATAFILE_E4).resample(4)

    # preprocessing
    eda_FC = lp_filter(eda_FC)
    eda_E4 = lp_filter(eda_E4)
    
    #
    sqi_fc = compute_sqis(eda_FC)
    sqi_e4 = compute_sqis(eda_E4)

    #
    np.savetxt(os.path.join(OUTDIR_FC, f'{SUB}.txt'), sqi_fc)
    np.savetxt(os.path.join(OUTDIR_E4, f'{SUB}.txt'), sqi_e4)    
    
#%%