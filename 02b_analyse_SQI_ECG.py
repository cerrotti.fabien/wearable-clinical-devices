import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd
from config import DATASET_FOLDER

#%%
subjects = os.listdir(os.path.join(DATASET_FOLDER, 'signals'))

metrics = ['kurtosis', 'skew', 'spectral', 'energy']

OUTDIR = os.path.join(DATASET_FOLDER, 'results')

#%%
SQI_FC_b = []
SQI_WD_b = []

for SUB in subjects:
    sqi_fc = np.loadtxt(os.path.join(DATASET_FOLDER, 'SQI', 'ECG' , 'baseline', 'FC', f'{SUB}.txt'))
    sqi_wd = np.loadtxt(os.path.join(DATASET_FOLDER, 'SQI', 'ECG' , 'baseline', 'HB', f'{SUB}.txt'))
    
    SQI_FC_b.append(sqi_fc)
    SQI_WD_b.append(sqi_wd)

#%
SQI_FC_m = []
SQI_WD_m = []    

for SUB in subjects:
    sqi_fc = np.loadtxt(os.path.join(DATASET_FOLDER, 'SQI', 'ECG' , 'movement', 'FC', f'{SUB}.txt'))
    sqi_wd = np.loadtxt(os.path.join(DATASET_FOLDER, 'SQI', 'ECG' , 'movement', 'HB', f'{SUB}.txt'))
    
    
    SQI_FC_m.append(sqi_fc)
    SQI_WD_m.append(sqi_wd)

#%%
index = 0
K_TH = 5
mean_values_fc_b = []
mean_values_wd_b = []

mean_values_fc_m = []
mean_values_wd_m = []

ratio_values_fc_b = []
ratio_values_wd_b = []

ratio_values_fc_m = []
ratio_values_wd_m = []

for sqi_fc_b, sqi_wd_b, sqi_fc_m, sqi_wd_m in zip(SQI_FC_b, SQI_WD_b, SQI_FC_m, SQI_WD_m):
    fc_b = sqi_fc_b[:,index]
    wd_b = sqi_wd_b[:,index]
    fc_m = sqi_fc_m[:,index]
    wd_m = sqi_wd_m[:,index]
    
    mean_values_fc_b.append(np.mean(fc_b))
    mean_values_wd_b.append(np.mean(wd_b))
    
    mean_values_fc_m.append(np.mean(fc_m))
    mean_values_wd_m.append(np.mean(wd_m))

    ratio_values_fc_b.append(np.sum(fc_b>K_TH)/len(fc_b)) #CHANGE
    ratio_values_wd_b.append(np.sum(wd_b>K_TH)/len(wd_b)) #CHANGE
    
    ratio_values_fc_m.append(np.sum(fc_m>K_TH)/len(fc_m)) #CHANGE
    ratio_values_wd_m.append(np.sum(wd_m>K_TH)/len(wd_m)) #CHANGE

#%%
table_mean = np.c_[mean_values_fc_b, mean_values_wd_b, mean_values_fc_m, mean_values_wd_m]
table_mean_pd = pd.DataFrame(table_mean, index = subjects, columns = ['FC_b', 'WD_b', 'FC_m', 'WD_m'])

table_ratio = np.c_[ratio_values_fc_b, ratio_values_wd_b, ratio_values_fc_m, ratio_values_wd_m]
table_ratio_pd = pd.DataFrame(table_ratio, index = subjects, columns = ['FC_b', 'WD_b', 'FC_m', 'WD_m'])

table_mean_pd.to_csv(os.path.join(OUTDIR, 'ECG_KUR_means.csv'), float_format = '%.2f')
table_ratio_pd.to_csv(os.path.join(OUTDIR,'ECG_KUR_ratio.csv'), float_format = '%.2f') #CHANGE

#
plt.figure()
ax1 = plt.subplot(121)
plt.title('Baseline')
plt.boxplot([mean_values_fc_b, mean_values_wd_b], labels = ['FC', 'HB']) #CHANGE
plt.hlines(K_TH, 0, 3, 'r')
plt.grid()

plt.subplot(122, sharey=ax1)
plt.title('Movement')
plt.boxplot([mean_values_fc_m, mean_values_wd_m], labels = ['FC', 'HB']) #CHANGE
plt.hlines(K_TH, 0, 3, 'r')
plt.grid()

plt.suptitle('Kurtosis')

#%% 
index=2
mean_values_fc_b = []
mean_values_wd_b = []

mean_values_fc_m = []
mean_values_wd_m = []

ratio_values_fc_b = []
ratio_values_wd_b = []

ratio_values_fc_m = []
ratio_values_wd_m = []

for sqi_fc_b, sqi_wd_b, sqi_fc_m, sqi_wd_m in zip(SQI_FC_b, SQI_WD_b, SQI_FC_m, SQI_WD_m):
    fc_b = sqi_fc_b[:,index]
    wd_b = sqi_wd_b[:,index]
    fc_m = sqi_fc_m[:,index]
    wd_m = sqi_wd_m[:,index]
    
    mean_values_fc_b.append(np.mean(fc_b))
    mean_values_wd_b.append(np.mean(wd_b))
    
    mean_values_fc_m.append(np.mean(fc_m))
    mean_values_wd_m.append(np.mean(wd_m))

    ratio_values_fc_b.append(np.sum((fc_b>=0.5) & (fc_b <=0.8))/len(fc_b))
    ratio_values_wd_b.append(np.sum((wd_b>=0.5) & (wd_b <=0.8))/len(wd_b))
    
    ratio_values_fc_m.append(np.sum((fc_m>=0.5) & (fc_m <=0.8))/len(fc_m))
    ratio_values_wd_m.append(np.sum((wd_m>=0.5) & (wd_m <=0.8))/len(wd_m))
    
#
table_mean = np.c_[mean_values_fc_b, mean_values_wd_b, mean_values_fc_m, mean_values_wd_m]
table_mean_pd = pd.DataFrame(table_mean, index = subjects, columns = ['FC_b', 'WD_b', 'FC_m', 'WD_m'])

table_ratio = np.c_[ratio_values_fc_b, ratio_values_wd_b, ratio_values_fc_m, ratio_values_wd_m]
table_ratio_pd = pd.DataFrame(table_ratio, index = subjects, columns = ['FC_b', 'WD_b', 'FC_m', 'WD_m'])

table_mean_pd.to_csv(os.path.join(OUTDIR,'ECG_SPR_means.csv'), float_format = '%.2f') #CHANGE
table_ratio_pd.to_csv(os.path.join(OUTDIR,'ECG_SPR_ratio.csv'), float_format = '%.2f') #CHANGE

#
plt.figure()

ax1 = plt.subplot(121)
plt.title('Baseline')
plt.boxplot([mean_values_fc_b, mean_values_wd_b], labels = ['FC', 'HB']) #CHANGE
plt.hlines(.5, 0, 3, 'r')
plt.hlines(0.8, 0, 3, 'r')
plt.grid()

plt.subplot(122, sharey=ax1)
plt.title('Movement')
plt.boxplot([mean_values_fc_m, mean_values_wd_m], labels = ['FC', 'HB']) #CHANGE
plt.hlines(.5, 0, 3, 'r')
plt.hlines(0.8, 0, 3, 'r')
plt.grid()

plt.suptitle('Spectral Power Ratio')